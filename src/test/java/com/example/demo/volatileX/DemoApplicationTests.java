package com.example.demo.volatileX;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.StringUtils;

@SpringBootTest
public class DemoApplicationTests {

	@Test
	void contextLoads() {
	}

	public static void main(String[] args) {
		//System.out.println("123".split(",")[0]);
		System.out.println(StringUtils.delimitedListToStringArray(null, ",").length);
	}

	private static class StaticInnerClass{

	}
	public class InnerClass{

	}
}
 class OuterClass{

}