package com.example.demo.volatileX;

/**
 *
 * 内存可见性问题,当多个线程共享数据时，彼此不可见
 * volatile 关键字: 当多个线程进行操作共享数据时,可以保证内存中的数据是可见的;相较于 synchronized 是一种
 * 较为轻量级的同步策略;
 * volatile 不具备"互斥性";
 * volatile 不能保证变量的"原子性";
 *
 * @author Andy Cui
 * @version 1.0
 * @date 2020/12/28 13:17
 */
public class VolatileTest {
    public static void main(String[] args){
        ThreadDemo td = new ThreadDemo();
        new Thread(td).start();

        while(true){
            if(td.isFlag()){
                System.out.println("########");
                break;
            }
        }
    }
}
//class ThreadDemo implements Runnable{
//    private boolean flag = false;
//
//    public void run(){
//        try{
//            // 该线程 sleep(200), 导致了程序无法执行成功
//            Thread.sleep(200);
//        }catch(InterruptedException e){
//            e.printStackTrace();
//        }
//
//        flag = true;
//
//        System.out.println("flag="+isFlag());
//    }
//
//    public boolean isFlag(){
//        return flag;
//    }
//
//    public void setFlag(boolean flag){
//        this.flag = flag;
//    }
//}


// 解决问题方式一: 同步锁
//   但是,效率太低
//public class TestVolatile{
//
//    public static void main(String[] args){
//        ThreadDemo td = new ThreadDemo();
//        new Thread(td).start();
//
//
//        while(true){
//            // 使用同步锁
//            synchronized(td){
//                if(td.isFlag()){
//                    System.out.println("########");
//                    break;
//                }
//            }
//        }
//    }
//}

// 解决方式二: 使用 volatile 关键字
class ThreadDemo implements Runnable{
    private volatile boolean  flag = false;

    public void run(){
        try{
            // 该线程 sleep(200), 导致了程序无法执行成功
            Thread.sleep(200);
        }catch(InterruptedException e){
            e.printStackTrace();
        }

        flag = true;

        System.out.println("flag="+isFlag());
    }

    public boolean isFlag(){
        return flag;
    }

    public void setFlag(boolean flag){
        this.flag = flag;
    }
}