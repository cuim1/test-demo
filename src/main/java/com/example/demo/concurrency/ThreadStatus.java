package com.example.demo.concurrency;

import java.util.concurrent.TimeUnit;

/**
 * @author Andy Cui
 * @version 1.0
 * @date 2020/12/31 14:44
 */
public class ThreadStatus {
    public static void main(String[] args) {
        //TIME_WAITING
//        new Thread(() -> {
//            while (true) {
//                System.out.println(Thread.currentThread().getName() + "->执行了run()");
//                try {
//                    TimeUnit.SECONDS.sleep(88);
//                } catch (InterruptedException e) {
//                    e.printStackTrace();
//                }
//            }
//        }, "thread-time-waiting").start();
//
//        //WAITING，线程在ThreadStatus类锁上通过wait进行等待
//        new Thread(() -> {
//            while (true) {
//                synchronized (ThreadStatus.class){
//                    System.out.println(Thread.currentThread().getName() + "->before wait()");
//                    try {
//                        ThreadStatus.class.wait();
//                        System.out.println(Thread.currentThread().getName() + "->after wait()");
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//
//            }
//        }, "thread-waiting").start();

         //WAITING，t线程执行join(),main线程等待
//        Thread t = new Thread(()->{
//            System.out.println("exec thread-sleep ...");
//            try {
//                TimeUnit.SECONDS.sleep(100);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//        });
//        t.start();
//        System.out.println("before thread-sleep join()");
//        try {
//            t.join();
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        System.out.println("after thread-sleep join()");
//        System.out.println("complete main() ");

        //WAITING，线程t1在ThreadStatus类锁上通过wait进行等待,t2调用notify()
//        Thread t1 = new Thread(() -> {
//                synchronized (ThreadStatus.class){
//                    System.out.println(Thread.currentThread().getName() + "->before wait()");
//                    try {
//                        ThreadStatus.class.wait(1000*60*1);
//                        System.out.println(Thread.currentThread().getName() + "->after wait()");
//                    } catch (InterruptedException e) {
//                        e.printStackTrace();
//                    }
//                }
//        }, "thread-wait");
//        Thread t2 = new Thread(() -> {
//            synchronized (ThreadStatus.class){
//                System.out.println(Thread.currentThread().getName() + "->before notify()");
//
//                ThreadStatus.class.notify();
//                System.out.println(Thread.currentThread().getName() + "->after notify()");
//            }
//        }, "thread-notify");
//
//        t1.start();
//
//        t2.start();

        // BLOCKED t1 和 t2争夺锁，造成一个线程阻塞
        new Thread(()->{
            synchronized (ThreadStatus.class){
                while(!Thread.currentThread().isInterrupted()){

                }
            }
        },"t1").start();
        new Thread(()->{
            synchronized (ThreadStatus.class){
                while(!Thread.currentThread().isInterrupted()){

                }
            }
        },"t2").start();
    }

}
