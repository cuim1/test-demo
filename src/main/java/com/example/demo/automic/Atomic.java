package com.example.demo.automic;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author Andy Cui
 * @version 1.0
 * @date 2020/12/28 14:29
 */
public class Atomic {
    public static void main(String[] args) {
//        问题一、i++问题  ，涉及到操作数栈的出入栈

//        int i = 10;
//        i = i++;  // 此时, i=10
//        //执行步骤:
//        int temp = i;
//        i = i + 1;
//        System.out.println("i = "+i);
//        i = temp;
//        System.out.println("i = "+i);
//        int i = 1;
//        i ++;
//        int k = i + ++i * i++;
//        System.out.println(k);   // 11 = 2 + 3 * 3

        // 问题二、线程安全
        AtomicDemo ad = new AtomicDemo();

        for(int i=0; i < 10; i++){
            new Thread(ad).start();
        }
    }
}
class AtomicDemo implements Runnable{
    //private int serialNumber = 0;
    private AtomicInteger serialNumber = new AtomicInteger();

    public void run(){
        System.out.println(Thread.currentThread().getName() + ":" + getSerialNumber());
    }

    public int getSerialNumber(){
        //return serialNumber++;
        return serialNumber.getAndIncrement();
    }
}
